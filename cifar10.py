# from https://tutorials.pytorch.kr/beginner/blitz/cifar10_tutorial.html
# https://github.com/9bow/PyTorch-tutorials-kr/blob/master/beginner_source/blitz/cifar10_tutorial.py

"""
이미지 분류기 학습하기
----------------------------
다음과 같은 단계로 진행해보겠습니다:
1. ``torchvision`` 을 사용하여 CIFAR10의 학습용 / 시험용 데이터셋을
   불러오고, 정규화(nomarlizing)합니다.
2. 합성곱 신경망(Convolution Neural Network)을 정의합니다.
3. 손실 함수를 정의합니다.
4. 학습용 데이터를 사용하여 신경망을 학습합니다.
5. 시험용 데이터를 사용하여 신경망을 검사합니다.

1. CIFAR10을 불러오고 정규화하기
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
``torchvision`` 을 사용하여 매우 쉽게 CIFAR10을 불러올 수 있습니다.
"""
import torch
import torchvision
import torchvision.transforms as transforms
import timeit
from torchvision.datasets import ImageFolder
from backbone.iresnet import iresnet18

# trainloader = torch.utils.data.DataLoader() 의 num_workers 가 1 이상이면 if __name__ == '__main__': 처리필요!!!
# num_workers=0 이거나 해당 인자를 생략하면 에러 발생하지 않음
# An attempt has been made to start a new process before the current process has finished its bootstrapping phase.

########################################################################
# torchvision 데이터셋의 출력(output)은 [0, 1] 범위를 갖는 PILImage 이미지입니다.
# 이를 [-1, 1]의 범위로 정규화된 Tensor로 변환합니다.
#
# .. note::
#     만약 Windows 환경에서 BrokenPipeError가 발생한다면,
#     torch.utils.data.DataLoader()의 num_worker를 0으로 설정해보세요.

BATCH_SIZE = 1024 #32
EPOCH_CNT = 10
PATH = './cifar_net.pth'
LOG_INTERVAL = 1000

USE_IMAGE_FOLDER = not True
USE_RESNET = True
USE_DP = True if USE_RESNET else False # on RTX 8000 with bs32, poor result(1/2 speed, acc)

NUM_CLASSES = 10
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

LR = 0.1

# [Quadro RTX 8000] .112 server, lr=0.001
# Net bs4=182s(init=2.252, ep1=1.519, ep2=1.459, Acc=56%) # batch size 가 4일 경우, loss & Acc 가 가장 좋음
# Net bs32=101s(init=2.328, ep1=1.375, ep2=1.860, Acc=48%) vs ImageFolder 가 2배이상 느림!
# Net bs128=90s(init=2.299, ep1=2.153, ep2=1.636, Acc=36%)
# iresnet18 bs32=899s(init=2.559, ep1=1.920, ep2=1.493, Acc=40%)
# iresnet18 bs2048=178s(init=2.705, ep1=1.714, ep2=1.514, Acc=46%)
#   vs lr=0.1 -> ep2=1.196(55%), ep10=0.392(75%), 651s(num_worker=0) / 407s(num_worker=4), ep10 (78%) 2184s(num_worker=0)
# iresnet18 bs3072=152s(init=2.705, ep1=1.735, ep2=1.631, Acc=44%)
#
# iresnet18 bs2048=236s, ep10 (Acc=74%)
# iresnet18 bs1024=251s, ep10 (Acc=77%)

def main():
  transform = transforms.Compose([
    transforms.Resize((112,112)), # for USE_RESNET
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    # bs32 기준 기존 40% 보다 6 ~ 8% 좋아짐
    # transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
  ])

  trainset = None
  testset = None

  if USE_IMAGE_FOLDER:
    img_trainset = ImageFolder(root='./data/cifar10_dataset', transform=transform)
    trainset, testset = torch.utils.data.random_split(img_trainset, [50000, 10000])
  else:
    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
  
  # print(len(trainset), len(testset)) # 50000 10000
  trainloader = torch.utils.data.DataLoader(trainset, batch_size=BATCH_SIZE, shuffle=True, num_workers=4)
  testloader = torch.utils.data.DataLoader(testset, batch_size=BATCH_SIZE, shuffle=False, num_workers=4)

  # 학습용 이미지를 무작위로 가져오기
  if not True:
    dataiter = iter(trainloader)
    images, labels = dataiter.next() # images = [4, 3, 32, 32]
    # del dataiter # 필요한가???

    # 이미지 보여주기
    # ~.make_grid(padding=2) 이므로 h=32+2*2=36, w=32*4+2*5=138, 즉 [4, 3, 32, 32] -> [3, 36, 138]
    imshow(torchvision.utils.make_grid(images))
    # 정답(label) 출력
    print(' '.join('%5s' % classes[labels[j]] for j in range(BATCH_SIZE)))

  train(trainloader)
  test(testloader)

def get_model(device):
  if USE_RESNET:
    net = iresnet18(num_features=NUM_CLASSES).to(device)
  else:
    net = Net().to(device)

  if USE_DP and torch.cuda.device_count() > 1:
    net = nn.DataParallel(net)
  return net

########################################################################
# 이미지를 보여주기 위한 함수

def imshow(img):
  import matplotlib.pyplot as plt
  import numpy as np

  img = img / 2 + 0.5 # unnormalize
  npimg = img.numpy() # img.shape = torch.Size([3, 36, 138]), npimg.shape = (3, 36, 138)
  plt.imshow(np.transpose(npimg, (1, 2, 0))) # (36, 138, 3) -> 이미지 4장(BATCH_SIZE)이 가로로 표시됨
  # plt.imshow(np.transpose(npimg)) # 이미지 4장이 세로로 표시됨, np.transpose() 안해주면 에러!
  plt.show()

########################################################################
# 2. 합성곱 신경망(Convolution Neural Network) 정의하기
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# 이전의 신경망 섹션에서 신경망을 복사한 후, (기존에 1채널 이미지만 처리하도록
# 정의된 것을) 3채널 이미지를 처리할 수 있도록 수정합니다.

import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self, num_classes=NUM_CLASSES):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 25 * 25, 120) # 32 x 32 -> (16 * 5 * 5, 120), 112 x 112 -> (16 * 25 * 25, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, num_classes) # num_classes = 10

    def forward(self, x): # x.shape = torch.Size([bs, 3, 112, 112])
        x = self.pool(F.relu(self.conv1(x))) # 112 x 112 conv1 -> 108 x 108 pool -> 54 x 54, [bs, 6, 54, 54]
        x = self.pool(F.relu(self.conv2(x))) # 54, 54 conv2 -> 50 x 50 pool -> 25 x 25, [bs, 16, 25, 25]
        x = torch.flatten(x, 1) # 배치를 제외한 모든 차원을 평탄화(flatten), [bs, 10000]
        x = F.relu(self.fc1(x)) # [bs, 120]
        x = F.relu(self.fc2(x)) # [bs, 84]
        x = self.fc3(x) # [bs, 10]
        return x

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# device = 'cpu'
print(device) # cuda:0 or cpu

net = get_model(device)

########################################################################
# 3. 손실 함수와 Optimizer 정의하기
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# 교차 엔트로피 손실(Cross-Entropy loss)과 모멘텀(momentum) 값을 갖는 SGD를 사용합니다.

import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=LR, momentum=0.9)

########################################################################
# 4. 신경망 학습하기
# ^^^^^^^^^^^^^^^^^^^^
#
# 이제 재미있는 부분이 시작됩니다.
# 단순히 데이터를 반복해서 신경망에 입력으로 제공하고, 최적화(Optimize)만 하면 됩니다.

def train(trainloader):
  start = timeit.default_timer() # skc add

  for epoch in range(EPOCH_CNT):   # 데이터셋을 수차례 반복합니다.

      running_loss = 0.0
      last_loss = 0.0
      for i, data in enumerate(trainloader, 0):
          # [inputs, labels]의 목록인 data로부터 입력을 받은 후;
          # inputs, labels = data
          inputs, labels = data[0].to(device), data[1].to(device)

          # 변화도(Gradient) 매개변수를 0으로 만들고
          optimizer.zero_grad()

          # 순전파 + 역전파 + 최적화를 한 후
          outputs = net(inputs) # [bs, 10]
          loss = criterion(outputs, labels)
          loss.backward()
          optimizer.step()

          # 통계를 출력합니다.
          running_loss += loss.item()
          last_loss = loss.item()
          if i == 0: print('Epoch %d initial loss: %.3f' % (epoch + 1, loss.item()))
          if (i + 1) % LOG_INTERVAL == 0:    # print every LOG_INTERVAL mini-batches
              print('Epoch %d (%5d / %d) loss: %.3f' % 
                (epoch + 1, i + 1, len(trainloader), running_loss / LOG_INTERVAL))
              running_loss = 0.0

          if not True: break # run once

      print('Epoch %d -> last loss: %.3f\n' % (epoch + 1, last_loss))

  print('Finished Training')

  stop = timeit.default_timer()
  print('train(len=%d) Time: %.2f s. ' % (len(trainloader), stop - start))

  ########################################################################
  # 학습한 모델을 저장해보겠습니다:

  torch.save(net.state_dict(), PATH)

########################################################################
# PyTorch 모델을 저장하는 자세한 방법은 `여기 <https://pytorch.org/docs/stable/notes/serialization.html>`_
# 를 참조해주세요.
#
# 5. 시험용 데이터로 신경망 검사하기
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#
# 지금까지 학습용 데이터셋을 2회 반복하며 신경망을 학습시켰습니다.
# 신경망이 전혀 배운게 없을지도 모르니 확인해봅니다.
#
# 신경망이 예측한 출력과 진짜 정답(Ground-truth)을 비교하는 방식으로 확인합니다.
# 만약 예측이 맞다면 샘플을 '맞은 예측값(correct predictions)' 목록에 넣겠습니다.
#
# 첫번째로 시험용 데이터를 좀 보겠습니다.

def test(testloader):
  dataiter = iter(testloader)
  images, labels = dataiter.next()
  # del dataiter

  # 이미지를 출력합니다.
  # imshow(torchvision.utils.make_grid(images))
  print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

  ########################################################################
  # 이제, 저장했던 모델을 불러오도록 하겠습니다 (주: 모델을 저장하고 다시 불러오는
  # 작업은 여기에서는 불필요하지만, 어떻게 하는지 설명을 위해 해보겠습니다):

  net = get_model(device)
  net.load_state_dict(torch.load(PATH))

  ########################################################################
  # 좋습니다, 이제 이 예제들을 신경망이 어떻게 예측했는지를 보겠습니다:

  outputs = net(images.to(device))

  ########################################################################
  # 출력은 10개 분류 각각에 대한 값으로 나타납니다. 어떤 분류에 대해서 더 높은 값이
  # 나타난다는 것은, 신경망이 그 이미지가 해당 분류에 더 가깝다고 생각한다는 것입니다.
  # 따라서, 가장 높은 값을 갖는 인덱스(index)를 뽑아보겠습니다:
  _, predicted = torch.max(outputs, 1)

  print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(4)))

  ########################################################################
  # 결과가 괜찮아보이네요.
  #
  # 그럼 전체 데이터셋에 대해서는 어떻게 동작하는지 보겠습니다.

  correct = 0
  total = 0

  start = timeit.default_timer()

  # 학습 중이 아니므로, 출력에 대한 변화도를 계산할 필요가 없습니다
  with torch.no_grad():
      for data in testloader:
          # images, labels = data
          images, labels = data[0].to(device), data[1].to(device)
          # 신경망에 이미지를 통과시켜 출력을 계산합니다
          outputs = net(images)
          # 가장 높은 값(energy)를 갖는 분류(class)를 정답으로 선택하겠습니다
          _, predicted = torch.max(outputs.data, 1) # outputs 로 해도 동일 결과
          total += labels.size(0)
          correct += (predicted == labels).sum().item()

  print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total))

  stop = timeit.default_timer()
  print('test(len=%d) Time: %.2f s. ' % (len(testloader), stop - start))

  ########################################################################
  # (10가지 분류 중에 하나를 무작위로) 찍었을 때의 정확도인 10% 보다는 나아보입니다.
  # 신경망이 뭔가 배우긴 한 것 같네요.
  #
  # 그럼 어떤 것들을 더 잘 분류하고, 어떤 것들을 더 못했는지 알아보겠습니다:

  # 각 분류(class)에 대한 예측값 계산을 위해 준비
  correct_pred = {classname: 0 for classname in classes}
  total_pred = {classname: 0 for classname in classes}

  # 변화도는 여전히 필요하지 않습니다
  with torch.no_grad():
      for data in testloader:
          # images, labels = data
          images, labels = data[0].to(device), data[1].to(device)
          outputs = net(images)
          _, predictions = torch.max(outputs, 1)
          # 각 분류별로 올바른 예측 수를 모읍니다
          for label, prediction in zip(labels, predictions):
              if label == prediction:
                  correct_pred[classes[label]] += 1
              total_pred[classes[label]] += 1


  # 각 분류별 정확도(accuracy)를 출력합니다
  for classname, correct_count in correct_pred.items():
      accuracy = 100 * float(correct_count) / total_pred[classname]
      print("Accuracy for class {:5s} is: {:.1f} %".format(classname,
                                                    accuracy))

########################################################################
# 자, 이제 다음으로 무엇을 해볼까요?
#
# 이러한 신경망들을 GPU에서 실행하려면 어떻게 해야 할까요?
#
# GPU에서 학습하기
# ----------------
# Tensor를 GPU로 이동했던 것처럼, 신경망 또한 GPU로 옮길 수 있습니다.
#
# 먼저 (CUDA를 사용할 수 있다면) 첫번째 CUDA 장치를 사용하도록 설정합니다:

# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# CUDA 기기가 존재한다면, 아래 코드가 CUDA 장치를 출력합니다:
# print(device)

########################################################################
# 이 섹션의 나머지 부분에서는 ``device`` 를 CUDA 장치라고 가정하겠습니다.
#
# 그리고 이 메소드(Method)들은 재귀적으로 모든 모듈의 매개변수와 버퍼를
# CUDA tensor로 변경합니다:
#
# .. code:: python
#
#     net.to(device)
#
#
# 또한, 각 단계에서 입력(input)과 정답(target)도 GPU로 보내야 한다는 것도 기억해야
# 합니다:
#
# .. code:: python
#
#         inputs, labels = data[0].to(device), data[1].to(device)
#
# CPU와 비교했을 때 어마어마한 속도 차이가 나지 않는 것은 왜 그럴까요?
# 그 이유는 바로 신경망이 너무 작기 때문입니다.
#
# **연습:** 신경망의 크기를 키워보고, 얼마나 빨라지는지 확인해보세요.
# (첫번째 ``nn.Conv2d`` 의 2번째 인자와 두번째 ``nn.Conv2d`` 의 1번째 인자는
# 같은 숫자여야 합니다.)
#
# **다음 목표들을 달성했습니다**:
#
# - 높은 수준에서 PyTorch의 Tensor library와 신경망을 이해합니다.
# - 이미지를 분류하는 작은 신경망을 학습시킵니다.
#
# 여러개의 GPU에서 학습하기
# -------------------------
# 모든 GPU를 활용해서 더욱 더 속도를 올리고 싶다면, :doc:`data_parallel_tutorial`
# 을 참고하세요.

if __name__ == '__main__':
    main()

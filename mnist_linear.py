import torch
from torchvision import datasets, transforms

torch.manual_seed(123) # 재현되고 있음
# np.random.seed(123) # 이 소스에서는 재현과 상관없음

# LR = 1e-3, transforms.Normalize((0.1307,), (0.3081,))
# T = 2867 , F = 7133
# epoch:   1/3 | step 100/391 train loss: 2.2552 | val loss 2.1726 | 6.464 sec
# T = 8034 , F = 1966
# epoch:   3/3 | step 300/391 train loss: 0.9916 | val loss 0.9556 | 5.342 sec
# Finished! 55.920 sec
# LR = 1e-1 일 경우, T = 9606 , F = 394 로 성능이 더 좋아짐! Finished! 71.625 sec

# USE_CNN, LR = 1e-3 은 LinearModel 성능과 비슷하나
# USE_CNN, LR = 1e-1 은 훨씬 좋아짐
# T = 8607 , F = 1393
# epoch:   1/3 | step 100/391 train loss: 0.8859 | val loss 0.4314 | 30.759 sec
# T = 9727 , F = 273
# epoch:   3/3 | step 300/391 train loss: 0.0837 | val loss 0.0916 | 32.686 sec
# Finished! 354.865 sec

USE_CNN = True
BATCH_SIZE = 128
LR = 1e-3
# LR = 1e-1
if USE_CNN: LR = 1e-1
NUM_EPOCHS = 3

# 1) Prepare dataset
mnist_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.1307,), (0.3081,)) # 이걸 해줘야 성능이 월등히 좋아진다(속도는 2배정도 느려짐)!!!
    # transforms.Normalize((0.5,), (0.5,)) # 안해준 것보다는 좋아짐
])

# C:\Users\skc08\anaconda3\envs\pytorch\Lib\site-packages\torchvision\datasets\mnist.py
# transform 을 안해주면, 아래 DataLoader 에서 로딩시(enumerate(train_loader)) PIL.Image.Image 타입이라 에러가 발생함
train_dataset = datasets.MNIST('./mnist_data', train=True, download=True, transform=mnist_transform)
test_dataset = datasets.MNIST('./mnist_data', train=False) # transform=mnist_transform 인자 유무에 상관없이 결과가 동일함
# print('train:', len(train_dataset), ', test:', len(test_dataset)) # train: 60000 , test: 10000

# mnist_transform 을 적용하지 않아도 mean, std 가 tensor(0.1307), tensor(0.3081) 임
print(train_dataset.data.float().mean()/255, train_dataset.data.float().std()/255)

train_dataset, val_dataset = torch.utils.data.random_split(train_dataset, [50000, 10000])
print(len(train_dataset), len(val_dataset), len(test_dataset)) # 50000, 10000, 10000

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=False)
val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=BATCH_SIZE, shuffle=False)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=False)

# Visualize dataset
if False:
  import matplotlib.pyplot as plt

  data = next(iter(train_loader)) # same with it = iter(train_loader); data = it.next()
  img, label = data # type(data) -> <class 'list'>, img & label -> <class 'torch.Tensor'>
  # print(img.shape, label.shape) # torch.Size([bs, 1, 28, 28]) torch.Size([bs])
  # print(type(img[0,:].data), type(img[0,:].data.numpy())) # <class 'torch.Tensor'> <class 'numpy.ndarray'>
  # plt.imshow(img[0][0]); plt.show() # 처음 1장만 표시

  ncols = 4
  sample_cnt = 8
  nrows = sample_cnt // ncols
  if sample_cnt % ncols > 0: raise ValueError('sample_cnt error!')
  fig, ax = plt.subplots(nrows, ncols) # , figsize=(6, 6)

  for i in range(nrows * ncols):
    row = i // ncols # / 로 나누면 float
    col = i % ncols
    ax[row, col].imshow(img[i,:].data.numpy().reshape(28, 28), cmap='Greys') # cmap='gray' 는 검은바탕에 흰글씨

  plt.show()

# 2) Define Model
import torch.nn as nn

class LinearModel(nn.Module):
  def __init__(self):
    super(LinearModel, self).__init__()
    # self.fc1.weight.shape = torch.Size([100/*out*/, 784/*in*/]) 이지만, self.fc1(x) 가 호출될 경우,
    # ~/torch/nn/modules/linear.py 의 nn.Linear.forward() 가 호출되면서 return F.linear(input, self.weight, self.bias)
    # F.linear() 는 ~/torch/nn/functional.py 에서 output = input.matmul(weight.t()) 로 계산되고 있음
    # from C:\Users\skc08\anaconda3\envs\pytorch\Lib\site-packages\torch\nn\functional.py:1692
    # or from https://github.com/pytorch/pytorch/blob/1b746b95fb6f3df45bd2883045de9fbd82b72100/torch/nn/functional.py#L1379
    self.fc1 = nn.Linear(in_features=28*28, out_features=100)
    self.fc2 = nn.Linear(100, 10)
    self.relu = nn.ReLU()

  def forward(self, x):
    x = self.fc1(x)   # x: (bs, 784) -> (bs, 100)
    x = self.relu(x)
    x = self.fc2(x)   # x: (bs, 100) -> (bs, 10)
    return x

class CNNModel(nn.Module):
    def __init__(self):
        super(CNNModel, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(9216, 128)
        self.fc2 = nn.Linear(128, 10)

    def forward(self, x):
        x = self.conv1(x)       # (bs, 1, 28, 28) -> (bs, 32, 26, 26)
        x = F.relu(x)
        x = self.conv2(x)       # (bs, 32, 26, 26) -> (bs, 64, 24, 24)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)  # (bs, 64, 24, 24) -> (bs, 64, 12, 12)
        x = self.dropout1(x)
        x = torch.flatten(x, 1) # (bs, 64, 12, 12) -> (bs, 9216)
        x = self.fc1(x)         # (bs, 9216) -> (bs, 128)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)         # (bs, 128) -> (bs, 10)
        return x

# 3) Train Model
import torch.optim as optim
import time
import torch.nn.functional as F

if USE_CNN: model = CNNModel()
else: model = LinearModel()

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=LR)

num_batches = len(train_loader) # 50,000 / 128(bs) = 390.625 -> 391 소수점 올림
print('num_epochs {}, num_batches {}'.format(NUM_EPOCHS, num_batches))

total_start = time.time()
for epoch in range(NUM_EPOCHS):
  trn_loss = 0
  start = time.time()
  for i, data in enumerate(train_loader):
    img, label = data # type(data) = <class 'list'>, img.shape = torch.Size([bs, 1, 28, 28]), label.shape = torch.Size([bs])
    if USE_CNN: output = model(img)
    else:
      input = img.view(-1, 28 * 28)   # same with img.reshape(-1, 28 * 28)
      output = model(input)           # input: torch.Size([bs, 784]), output: (bs, 10)
    # *** 중요 *** 실수로 nn.CrossEntropyLoss() 전에 이렇게 F.softmax() 를 호출하면 성능에 악영향을 미친다.
    # output = F.softmax(output, dim=1) # output[0].sum() = 1.0
    loss = criterion(output, label) # output: (bs, 10), label: (bs,)
    trn_loss += loss
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if (i + 1) % 100 == 0:
      with torch.no_grad(): # 메모리 사용량을 줄이고 속도를 높인다함(하지만 측정해보면 별차이 안남)
        true_cnt = false_cnt = 0
        val_loss = 0.
        for j, val in enumerate(val_loader): # 10,000 / bs(128) = 78.125 -> 79
          img, label = val
          if USE_CNN: output = model(img)
          else:
            input = img.reshape(-1, 28 * 28)  # same with img.view(-1, 28 * 28)
            output = model(input)
          loss = criterion(output, label)
          val_loss += loss
          for k in range(len(output)): # len(output) = bs(128)
            if label[k].equal(torch.argmax(output[k])):   true_cnt += 1
            else: false_cnt += 1
        end = time.time()
        print('T =', true_cnt, ", F =", false_cnt)
        print('epoch: {:3d}/{} | step {:3d}/{} train loss: {:.4f} | val loss {:.4f} | {:.3f} sec\n'
          .format(epoch + 1, NUM_EPOCHS, i + 1, num_batches, trn_loss / 100, val_loss / len(val_loader), end - start))
        trn_loss = 0
        start = end

print('Finished! {:.3f} sec'.format(time.time() - total_start))

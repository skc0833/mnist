# from https://ndb796.tistory.com/373
import torch
import torchvision
import torchvision.transforms as transforms
from PIL import Image
from matplotlib.pyplot import imshow

transform_train = transforms.Compose([
    transforms.ToTensor(),
])

num_classes = 10
number_per_class = {}

def main():
    train_dataset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform_train)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=128, num_workers=4)
    test_dataset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform_train)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=128, num_workers=4)

    for i in range(num_classes):
        number_per_class[i] = 0

    make_image_folder(train_loader)
    make_image_folder(test_loader)

    img = Image.open('./data/cifar10_dataset/0/0.jpg')
    imshow(np.asarray(img))

    # test_image_folder(train_loader)

import os
import matplotlib.image as image
import numpy as np

def custom_imsave(img, label):
    path = './data/cifar10_dataset/' + str(label) + '/'
    if not os.path.exists(path):
        os.makedirs(path)
    img = img.numpy()
    img = np.transpose(img, (1, 2, 0))
    image.imsave(path + str(number_per_class[label]) + '.jpg', img)
    number_per_class[label] += 1

def make_image_folder(train_loader):
    for batch_idx, (inputs, targets) in enumerate(train_loader):
        print("[ Current Batch Index: " + str(batch_idx) + " ]")
        for i in range(inputs.size(0)):
            custom_imsave(inputs[i], targets[i].item())


# ImageFolder 라이브러리를 이용해서, 우리가 저장한 이미지들을 이용해 다시 PyTorch 데이터셋 객체로 불러올 수 있는지 확인

import matplotlib.pyplot as plt
def custom_imshow(img):
    img = img.numpy()
    # [Batch Size, Channel, Width, Height] -> [Width, Height, Channel]
    plt.imshow(np.transpose(img, (1, 2, 0)))
    plt.show()

def test_image_folder(train_loader):
    for batch_idx, (inputs, targets) in enumerate(train_loader):
        custom_imshow(inputs[0])

if __name__ == '__main__':
    main()
